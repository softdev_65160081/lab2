/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import static com.mycompany.lab2.Lab2.checkCol;
import java.util.Scanner;

/**
 *
 * @author Diarydear
 */
public class Lab2 {
    
    static char[][] table = {{'-','-','-'},
    {'-','-','-'},
    {'-','-','-'},
    };
    
    static char currentPlayer = 'X';
    static int row, col;
    
    static void printWelcome(){
        System.out.println("Welcome to OX");
    }

    static void printTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }
    
    static void setTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
               table[i][j] = '-';
            }
        }
    }
     
    static void printTurn(){
        System.out.println(currentPlayer+" Turn");
    }
    
    static void inputRowCol(){
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input row, col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if(table[row-1][col -1] =='-'){
                table[row-1][col -1] = currentPlayer;
                break;
            }
        }
    }
    
    static void switchPlayer(){
   
        if (currentPlayer == 'X'){
            currentPlayer = 'O';
        }
        else{
            currentPlayer = 'X';
        }
    }
    
    static boolean isWin() {
        if (checkRow() || checkCol() || checkD1() || checkD2()) {
            return true;
        }
        return false;
    }
    
    static boolean checkRow(){
        for(int i=0;i<3;i++){
            if(table[row -1 ][i]!=currentPlayer){
                return false;
            }
        }
        return true;
    }
    
    static boolean isDraw(){
       for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(table[i][j]=='-'){
                    return false;
                }
            }
        }
        return true;
    }
    
    static boolean checkCol(){
        for(int j=0;j<3;j++){
            if(table[j ][col-1]!=currentPlayer){
                return false;
            }
        }
        return true;
    }
    
    static boolean checkD1(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(table[i][i]!=currentPlayer){
                    return false;
                }
            }
        }
        return true;
    }
    
    static boolean checkD2(){
        for(int i=0;i<3;i++){
            if((table[0][2]==currentPlayer) 
                    && (table[1][1]==currentPlayer) 
                    && (table[2][0]==currentPlayer)) {
                return true;
            }
        }
        return false;
    }
    
    static char cont = 'y' ;
        static void prinContinue(){
            System.out.print("Continue (y/n): ");
            Scanner sc = new Scanner(System.in);
            char c = sc.next().charAt(0);  
            cont = c;
      }

    static boolean checkCont() {
        if (cont == 'y') {
            return true;
        }
        return false;
    }
    
    static void printWin() {
        System.out.println(currentPlayer + " Win!!");
    }

    static void printDraw() {
        System.out.println("Draw!!");
    }

    static void printEnd() {
        System.out.println("End game");

    }
     
    public static void main(String[] args) {
 
        printWelcome(); 
      while(checkCont()){ 
           printTable();
           printTurn();
           inputRowCol();

        if(isWin()){
            printTable();
            printWin();
            setTable();
            prinContinue();

        }
        
        if (isDraw()) {
            printTable();
            printDraw();
            setTable();
            prinContinue();
        }
        
        switchPlayer();
                
        }
        printEnd();
      
    }

}
